'''
Created on 3/12/2014

@author: Jorge
'''
from algoritmia.schemes.divideandconquer import DivideAndConquerSolver, IDivideAndConquerProblem     
import sys

class Torneo(IDivideAndConquerProblem):
    def __init__(self, inicio, fin):  
        self.inicio = inicio
        self.fin = fin
        self.jugadores = fin - inicio
    
    def is_simple(self):    
        return self.jugadores == 2
      
    def trivial_solution(self):    
        return [(1, self.inicio, self.inicio + 1)]
    
    def divide(self):
        medio = (self.inicio + self.fin) // 2
        yield Torneo(self.inicio, medio)
        yield Torneo(medio, self.fin)
    
    def combine(self, subproblemas):   
        grupoA, grupoB = subproblemas 
        combinacion = sorted(grupoA + grupoB)
        for dia in range(self.jugadores // 2, self.jugadores):
            for i in range(0, self.jugadores // 2):
                jugador1 = i + self.inicio
                jugador2 = (i + dia) % (self.jugadores // 2) + ((self.inicio + self.fin) // 2)
                combinacion.append((dia, jugador1, jugador2))       
        return combinacion

def crearTorneo(inicio, fin):
    jugadores = fin - inicio
    if jugadores == 2:
        return [(1, inicio, inicio + 1)]
    medio = (inicio + fin) // 2
    grupoA = crearTorneo(inicio, medio)
    grupoB = crearTorneo(medio, fin)
    combinacion = grupoA + grupoB
    for dia in range(jugadores // 2, jugadores):
        for i in range(0, jugadores // 2):
            jugador1 = i + inicio
            jugador2 = (i + dia) % (jugadores // 2) + medio
            combinacion.append((dia, jugador1, jugador2))  
    return sorted(combinacion)

numero_jugadores = int(sys.argv[1])
calendario = DivideAndConquerSolver().solve(Torneo(0, numero_jugadores)) 
# calendario = crearTorneo(0, numero_jugadores)

dia_actual = 0
for partido in calendario:
    if partido[0] != dia_actual:
        print('Day ' + str(partido[0]) + ':')
        dia_actual = partido[0]
    print('\tp' + str(partido[1] + 1) + ' vs p' + str(partido[2] + 1))

