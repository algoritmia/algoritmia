'''
Created on 23/10/2014

@author: aalgorta
'''
from easycanvas import EasyCanvas
from algoritmia.datastructures.digraphs import UndirectedGraph
from algoritmia.data.iberia import iberia, km, coords
from tkinter import LAST

from algoritmia.problems.shortestpaths.positive.dijkstra import DijkstraShortestPathsFinder
from algoritmia.problems.shortestpaths.backtracer import Backtracer

capitales_provincia = set(['A Coruña', 'Alacant', 'Albacete', 'Almería', 'Ávila', 'Badajoz', 'Barcelona', 'Bilbao', 'Burgos', 'Castelló de la Plana', 'Ciudad Real', 'Cuenca', 'Cáceres', 'Cádiz', 'Córdoba', 'Donostia', 'Girona', 'Granada', 'Guadalajara', 'Huelva', 'Huesca', 'Jaén', 'León', 'Lleida', 'Logroño', 'Lugo', 'Madrid', 'Murcia', 'Málaga', 'Ourense', 'Oviedo', 'Palencia', 'Pamplona', 'Pontevedra', 'Salamanca', 'Santander', 'Segovia', 'Sevilla', 'Soria', 'Tarragona', 'Teruel', 'Toledo', 'Valladolid', 'València', 'Vitoria', 'Zamora', 'Zaragoza'])

class E2Viewer(EasyCanvas):
    def __init__(self, longitud, recorrido, window_size=(800,800)):
        EasyCanvas.__init__(self)

        self.longitud = longitud
        self.recorrido = recorrido
        self.right = max(coords[p][0] for p in iberia.V)
        self.top = min(coords[p][1] for p in iberia.V)
        self.left = min(coords[p][0] for p in iberia.V)
        self.bottom = max(coords[p][1] for p in iberia.V)
        self.window_size = window_size
        
        self.error = None
        if len(set(self.recorrido).intersection(capitales_provincia)) != len(capitales_provincia) or len(capitales_provincia) != len(self.recorrido):
            self.error = "No es un recorrido válido"
        
        dspf = DijkstraShortestPathsFinder()
        self.bps = {}
        for c in capitales_provincia:
            self.bps[c] = Backtracer(dspf.some_to_some_backpointers(iberia, km, [c], capitales_provincia))

    def main(self):
        rows = self.bottom-self.top+1
        cols = self.right-self.left+1
        node_size = min(self.window_size)/300  
        mx = abs(self.left-self.right)*0.02
        my = abs(self.top-self.bottom)*0.02
        self.window_size = (self.window_size[0], self.window_size[0]*(abs(self.top-self.bottom)+2*my)/(abs(self.left-self.right)+2*mx))
        self.easycanvas_configure(title = "E2 Viewer",
                                  background = 'white',
                                  size = self.window_size, 
                                  coordinates = (self.left-mx,self.bottom+my,self.right+mx,self.top-my))
         
        for u2,v2 in iberia.E:
            u = coords[u2]
            v = coords[v2]
            self.create_line(u[0], u[1], v[0], v[1], "gray")
         
        for u2 in iberia.V:
            u = coords[u2]
            self.create_filled_circle(u[0], u[1], node_size, relleno='palegreen')
            
        for u2 in capitales_provincia:
            u = coords[u2]
            self.create_filled_circle(u[0], u[1], node_size*1.5, relleno='red')
          
        try:
            direct = False
            if not direct:
                previo = self.recorrido[0]
                r = [previo]
                for c in self.recorrido[1:]+[self.recorrido[0]]:
                    r.extend(self.bps[previo].backtrace(c)[1:])
                    previo = c    
                self.recorrido = r
                
            dist = 0  
            if len(self.recorrido)>1:
                previo = self.recorrido[0]
                
                for capital in self.recorrido[1:]+[self.recorrido[0]]:
                    if not direct:
                        dist += km(previo, capital) 
                    else:
                        dist += self.bps[previo].distance(capital, km)
                    u, v = coords[previo], coords[capital]
                    self.create_line(u[0], u[1], v[0], v[1], "blue", arrow=LAST, dash=(3,3))
                    previo = capital
        except Exception as e:
            pass
        
        if self.error == None and abs(dist-self.longitud)>1:
            self.error = "La longitud indicada ({0:.3f}km) no es la correcta ({1:.3f}km)".format(self.longitud, dist)   
                
        if self.error != None:
            self.create_text(self.right, self.bottom, self.error, 16, anchor='SE', color='red')
        else:
            self.create_text(self.right, self.bottom*0.9, "Recorrido válido", 16, anchor='SE')
            self.create_text(self.right, self.bottom, "Longitud del recorrido: {0:.3f}km".format(dist), 16, anchor='SE')
        # Wait for a key   
        self.readkey(True)
        
if __name__ == '__main__':
    viewer = E2Viewer(0, [], window_size=(800, 800))
    viewer.run()