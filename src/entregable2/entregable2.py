'''
Created on 29/10/2014

@author: Jorge
'''
import sys
from algoritmia.utils import infinity
from algoritmia.data.iberia import iberia, km
from algoritmia.problems.shortestpaths.positive import DijkstraShortestPathsFinder
from e2_viewer import E2Viewer


# Calcula la distancia_desde_final desde un vertice al inicio del grafo.
# Recibe tambien el minimo ya encontrado, asi, si la distancia_desde_final
# que estamos calculando ya es mayor otra, dejamos de calcular.
def distancia_a_inicio(caminos, capital, minimo_encontrado=infinity):
    bp = {}  
    for d, o in caminos:
        bp[d] = o  
    distancia = 0
    while capital != bp[capital]:
        if distancia > minimo_encontrado:
            return infinity
        distancia += km(capital, bp[capital])
        capital = bp[capital]         
    return distancia

# Calcula la capital cuya suma de distancias a todas las otras es minima
def capital_de_inicio(capitales_provincia):
    longitud_minima = infinity
    capital_minima = capitales_provincia[0]
    capitales = set(capitales_provincia)
    for capital in capitales_provincia:
        inicio = set([capital])
        caminos = list(dij.some_to_some_backpointers(iberia, km, inicio, capitales))
        longitud = sum(distancia_a_inicio(caminos, otra_capital) for otra_capital in capitales_provincia)
        if longitud < longitud_minima:
            longitud_minima = longitud
            capital_minima = capital
    return capital_minima

#Devuelve la ciudad y la distancia minima a la capital indicada.
def ciudad_y_distancia_minima(capital, capitales, visitados):
    distancia_minima = infinity 
    inicio = set([capital])  
    caminos = list(dij.some_to_some_backpointers(iberia, km, inicio, capitales))
    for otra_capital in capitales_provincia:
        if otra_capital not in visitados:            
            distancia = distancia_a_inicio(caminos, otra_capital, distancia_minima)
            if distancia < distancia_minima:
                distancia_minima = distancia
                ciudad_minima = otra_capital
    return(distancia_minima, ciudad_minima)

capitales_provincia = ['A Coruña', 'Alacant', 'Albacete', 'Almería', 'Ávila', 'Badajoz', 'Barcelona', 'Bilbao',
                       'Burgos', 'Castelló de la Plana', 'Ciudad Real', 'Cuenca', 'Cáceres', 'Cádiz', 'Córdoba',
                       'Donostia', 'Girona', 'Granada', 'Guadalajara', 'Huelva', 'Huesca', 'Jaén', 'León',
                       'Lleida', 'Logroño', 'Lugo', 'Madrid', 'Murcia', 'Málaga', 'Ourense', 'Oviedo', 'Palencia',
                       'Pamplona', 'Pontevedra', 'Salamanca', 'Santander', 'Segovia', 'Sevilla', 'Soria', 'Tarragona',
                       'Teruel', 'Toledo', 'Valladolid', 'València', 'Vitoria', 'Zamora', 'Zaragoza']


dij = DijkstraShortestPathsFinder()

capital_inicial = capital_de_inicio(capitales_provincia)
capitales = set(capitales_provincia)

longitud = 0
recorrido = list([capital_inicial])

# Mientras que queden capitales por visitar, calcula la distancia_desde_final de la capital
# que esta mas proxima, la añade al recorrido y repite el proceso desde esa nueva capital
# En la ultima iteracion del bucle, suma la distancia_desde_final entre la ultima capital visitada
# y la primera.
while len(recorrido) < len(capitales_provincia):
    
    distancia_minima = infinity   
    distancia_minima_desde_inicio, ciudad_minima_al_inicio = ciudad_y_distancia_minima(recorrido[0], capitales, recorrido)
    distancia_minima_desde_final, ciudad_minima_al_fin = ciudad_y_distancia_minima(recorrido[-1], capitales, recorrido)
    
    if distancia_minima_desde_inicio < distancia_minima_desde_final:
        recorrido.insert(0, ciudad_minima_al_inicio)
        longitud+= distancia_minima_desde_inicio
    else:
        recorrido.append(ciudad_minima_al_fin)
        longitud+= distancia_minima_desde_final    

inicio = set([recorrido[0]])
fin= recorrido[-1]
caminos = list(dij.some_to_some_backpointers(iberia, km, inicio, capitales))       
longitud += distancia_a_inicio(caminos, fin)
    
# Salida con el formato que se pide    
print(longitud)
for capital in recorrido:
    print(capital)
  
# Visualiza el mapa y el recorrido en caso de que se especifique desde consola
# con '-g'
if len(sys.argv) == 2 and sys.argv[1] == '-g': 
    viewer = E2Viewer(longitud, recorrido, window_size=(800, 800))
    viewer.run()
