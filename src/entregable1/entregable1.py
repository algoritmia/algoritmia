'''
Created on 10/10/2014

@author: Jorge
'''
import sys
from algoritmia.datastructures.digraphs.digraph import UndirectedGraph
#from labyrinthviewer import LabyrinthViewer
from algoritmia.datastructures.queues import Fifo


def load_labyrinth(fichero):
    matriz = list()
    
    f = open(fichero)  
    for linea in f:
        matriz.append(linea.split(' '))
    f.close()
    
    filas = len(matriz)
    columnas = len(matriz[0])
    e = list()
    for i in range(filas):
        for j in range(columnas):
            if i < filas - 1 and 's' not in matriz[i][j]:
                e.append(((i, j), (i + 1, j)))
            if j < columnas - 1 and 'e' not in matriz[i][j]:
                e.append(((i, j), (i, j + 1)))
    
    return UndirectedGraph(E=e)

def puntomaslejano(grafo, v_inicial):

    queue = Fifo()
    seen = set()
    empates = list()
    distmax = 0
    
    queue.push((v_inicial, 0))
    seen.add(v_inicial)
    
    while len(queue) > 0:
        v, dist = queue.pop()
        for suc in grafo.succs(v):
            if suc not in seen:
                seen.add(suc)
                nuevadist = dist + 1
                queue.push((suc, nuevadist))
                if nuevadist == distmax:
                    empates.append(suc)
                if nuevadist > distmax:
                    distmax = nuevadist
                    empates = [suc]
   
    minv = empates[0]
    for i in range(1, len(empates)):
        minv = min(minv, empates[i])
           
    return (minv, dist)

def recorredor_aristas_anchura(grafo, v_inicial):    
    
    aristas = []
    queue = Fifo()
    seen = set()
    
    queue.push((v_inicial, v_inicial))
    seen.add(v_inicial)
    
    while len(queue) > 0:
        u, v = queue.pop()
        aristas.append((u, v))
        for suc in grafo.succs(v):
            if suc not in seen:
                seen.add(suc)
                queue.push((v, suc))
    
    return aristas
 


def recuperador_camino(lista_aristas, v):
    
    bp = {}
    camino = []
    
    for o, d in lista_aristas:
        bp[d] = o
    
    camino.append(v)
    while v != bp[v]:
        v = bp[v]
        camino.append(v)
        
    camino.reverse()
    
    return camino

if(len(sys.argv) > 1):
    
    laberinto = load_labyrinth(sys.argv[1])
    
    v, dist = puntomaslejano(laberinto, (0, 0))
    u, dist = puntomaslejano(laberinto, v)
    
    a, b = sorted((v, u))
    
    print(str(a[0]) + " " + str(a[1]))
    print(str(b[0]) + " " + str(b[1]))
    print(dist)
    
    if len(sys.argv) == 3 and sys.argv[2] == '-g': 
               
        aristas = recorredor_aristas_anchura(laberinto, a)
        camino = recuperador_camino(aristas, b)
       # lv = LabyrinthViewer(laberinto, cell_size=30, margin=10)
        
        lv.set_input_point(a)
        lv.set_output_point(b)
        lv.add_path(camino, 'blue')
        lv.run()
else:
    
    print("Se ha de especificar el nombre del fichero que contiene el laberinto")
    exit(-1)   
