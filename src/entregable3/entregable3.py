'''
Created on 14/11/2014

@author: Jorge
'''
from partialsolution import PartialSolutionWithOptimization 
from sokoban_solution_viewer import SokobanViewer, read_sokoban_puzle

from algoritmia.utils import argmin
import sys

def posibles(mat, player_pos, blocks_source, blocks_target):
    
    posibles = ['U', 'D', 'L', 'R']
    filas = len(mat) - 1
    columnas = len(mat[0]) - 1
    fila, columna = player_pos 
    
    # Mirar arriba    
    hayPared = mat[fila - 1][columna] == '#'
    hayBloque = (fila - 1, columna) in blocks_source
    noSePuedeDesplazar = fila - 2 > 0 and ((fila - 2, columna) in blocks_source or mat[fila - 2][columna] == '#')
    sePoneEnEsquina = fila - 3 > 0 and (mat[fila - 3][columna] == '#' and (mat[fila - 2][columna - 1] == '#' or mat[fila - 2][columna + 1] == '#'))
    noEsDestino = fila - 2 > 0 and ((fila - 2, columna) not in blocks_target)
    if hayPared or (hayBloque and (noSePuedeDesplazar or (sePoneEnEsquina and noEsDestino))):
        posibles.remove('U') 

    # Mirar abajo 
    hayPared = mat[fila + 1][columna] == '#'
    hayBloque = (fila + 1, columna) in blocks_source
    noSePuedeDesplazar = fila + 2 < filas and ((fila + 2, columna) in blocks_source or mat[fila + 2][columna] == '#')
    sePoneEnEsquina = fila + 3 < filas and (mat[fila + 3][columna] == '#' and (mat[fila + 2][columna - 1] == '#' or mat[fila + 2][columna + 1] == '#'))
    noEsDestino = fila + 2 > filas and ((fila + 2, columna) not in blocks_target)
    if hayPared or (hayBloque and (noSePuedeDesplazar or (sePoneEnEsquina and noEsDestino))):    
        posibles.remove('D')
     
    # Mirar izquierda 
    hayPared = mat[fila][columna - 1] == '#'
    hayBloque = (fila, columna - 1) in blocks_source
    noSePuedeDesplazar = columna - 2 > 0 and ((fila, columna - 2) in blocks_source or mat[fila][columna - 2] == '#')
    sePoneEnEsquina = columna - 3 > 0 and (mat[fila][columna - 3] == '#' and (mat[fila + 1][columna - 2] == '#' or mat[fila - 1][columna - 2] == '#'))
    noEsDestino = columna - 2 > 0 and ((fila, columna - 2) not in blocks_target)
    if hayPared or (hayBloque and (noSePuedeDesplazar or (sePoneEnEsquina and noEsDestino))):         
        posibles.remove('L')
    
    # Mirar derecha 
    hayPared = mat[fila][columna + 1] == '#'
    hayBloque = (fila, columna + 1) in blocks_source
    noSePuedeDesplazar = columna + 2 < columnas and ((fila, columna + 2) in blocks_source or mat[fila][columna + 2] == '#')
    sePoneEnEsquina = columna + 3 < columnas and (mat[fila][columna + 3] == '#' and (mat[fila + 1][columna + 2] == '#' or mat[fila - 1][columna + 2] == '#'))
    noEsDestino = columna + 2 > columnas and ((fila, columna + 2) not in blocks_target)
    if hayPared or (hayBloque and (noSePuedeDesplazar or (sePoneEnEsquina and noEsDestino))):     
        posibles.remove('R')

    return posibles

def sokoban_solver(player_pos, mat, blocks_source, blocks_target):
    
    class SokobanPS(PartialSolutionWithOptimization):
        
        def __init__(self, player_pos, blocks_source, decisions):
            self.player_pos = player_pos
            self.blocks_source = blocks_source
            self.decisions = decisions
        
        def is_solution(self):
            return len(set(self.blocks_source).union(set(blocks_target))) == len(blocks_target)
        
        def get_solution(self):
            return self.decisions
   
        def successors(self):      
            direcciones = posibles(mat, self.player_pos, self.blocks_source, blocks_target)
            for direccion in direcciones:        
                fila = self.player_pos[0]
                columna = self.player_pos[1]
                fila_bloque = fila
                columna_bloque = columna
                
                if direccion == 'U':
                    fila -= 1
                    fila_bloque -= 2
                elif direccion == 'D':
                    fila += 1
                    fila_bloque += 2
                elif direccion == 'L':
                    columna -= 1
                    columna_bloque -= 2
                elif direccion == 'R':
                    columna += 1
                    columna_bloque += 2   
    
                new_decisions = self.decisions + direccion
                new_posicion = (fila, columna)             
                new_blocks_source = self.blocks_source[:]
                
                if new_posicion in new_blocks_source:
                    new_blocks_source.remove(new_posicion)
                    new_blocks_source.append((fila_bloque, columna_bloque)) 
                    
                yield SokobanPS(new_posicion, new_blocks_source, new_decisions)
        
        def state(self):
            return (self.player_pos, tuple(self.blocks_source))
        
        def f(self):
            return len(self.decisions)
        
    def bt(ps):
        best_seen[ps.state()] = ps.f()
        if ps.is_solution():
            yield ps.get_solution()
        else:
            for new_ps in ps.successors():
                state = new_ps.state()
                if state not in best_seen or new_ps.f() < best_seen[state]:
                    yield from bt(new_ps)
    
               
    best_seen = {}
    decisions = ''
    initial_ps = SokobanPS(player_pos, blocks_source, decisions)
    yield from bt(initial_ps)

if len(sys.argv) == 2:
    level_file = sys.argv[1]
    
    player_pos, mat, blocks_source, blocks_target = read_sokoban_puzle(level_file)
    decisiones = sokoban_solver(player_pos, mat, blocks_source, blocks_target)
    best_decision = argmin(decisiones, lambda decision: len(decision))
    
    if best_decision != None:
        print(best_decision)
        sokoView = SokobanViewer(level_file, best_decision, 40, 10)
        sokoView.run()
    else:
        print('No se ha encontrado ninguna solucion a este puzzle')

else:
    print('No has introducido los datos correctamente. \n\tFormato: entregable3.py <ruta del archivo>')
