'''
Created on 27/09/2014

@author: David Llorens (dllorens@uji.es)  
'''

from easycanvas import EasyCanvas

def read_sokoban_puzle(filename):
    try:    
        level = open(filename, "r").read()
    except:
        print("Problem reading file level:", filename)
        sys.exit()
    player_pos, mat, blocks_source, blocks_target = None, [], [], set()  
    for lin in level.split('\n'):
        mat.append(list(lin))
    for r in range(len(mat)):
        for c in range(len(mat[0])):
            if mat[r][c] == 'p':
                mat[r][c] = ' '
                player_pos = (r,c)
            elif mat[r][c] == 'x':
                mat[r][c] = ' '
                blocks_target.add((r,c))
            elif mat[r][c] == 'o':
                mat[r][c] = ' '
                blocks_source.append((r,c))
    return player_pos, mat, blocks_source, blocks_target

class SokobanViewer(EasyCanvas):
    def __init__(self, level_file, decisions=[], cell_size=40, margin=10):
        self.player_pos, self.walls, self.blocks_source, self.blocks_target = read_sokoban_puzle(level_file)
        EasyCanvas.__init__(self)
        self.decisions = decisions
        self.cell_size = cell_size
        self.margin = margin
        self.max_col = len(self.walls[0])
        self.max_row = len(self.walls)
        
    def draw_player(self, x, y, size):
        def sq(py, px, color):
            xx = x0+px*s2+s22
            yy = y0+py*s2+s22
            self.create_filled_rectangle(xx-s22, yy-s22, xx+s22, yy+s22, "black", color, tags="player")
        s2 = size/5.0
        s22 = s2/2.0
        x0 = x-size/2.0
        y0 = y-size/2.0
        sq(0,2,"orange")
        sq(1,0,"orange");sq(1,1,"green");sq(1,2,"green");sq(1,3,"green");sq(1,4,"orange");
        sq(2,1,"green");sq(2,2,"green");sq(2,3,"green")
        sq(3,1,"darkgreen");sq(3,2,"darkgreen");sq(3,3,"darkgreen")
        sq(4,1,"orange");sq(4,3,"orange")
    
    def draw_state(self, first=False):
        walls = self.walls
        if not first:
            self.erase("player")
            self.erase("blocks")
            self.erase("targets") 
        for r in range(self.max_row):
            for c in range(self.max_col):
                x = c*self.cell_size
                y = r*self.cell_size
                x2 = (c+1)*self.cell_size
                y2 = (r+1)*self.cell_size
                xc, yc = (x+x2)/2, (y+y2)/2
                s = self.cell_size
                #draw walls
                if first:
                    self.create_filled_rectangle(x, y, x2, y2, "black", "blue" if walls[r][c]=='#' else "white")
                
                if (r, c) in self.blocks_source:
                    self.create_filled_rectangle(xc-s*0.4, yc-s*0.4, xc+s*0.4, yc+s*0.4, "black", "gray", tags="blocks")
                if (r, c) in self.blocks_target:
                    self.create_circle(xc, yc, self.cell_size/3, "black", tags="targets")
                if (r, c) == self.player_pos:
                    self.draw_player(xc, yc, self.cell_size*0.8)
       
    def faux3(self, list, elem):
        return list.index(elem) if elem in list else -1

    def faux1(self, dir):
        r, c = self.player_pos
        walls = self.walls
        numRows = len(walls) 
        numCols = len(walls[0])
        if dir=='U':
            if r==0 or walls[r-1][c] == '#' or (r-2>=0 and (r-1,c) in self.blocks_source and (walls[r-2][c] == '#' or (r-2,c) in self.blocks_source)):
                return False
        elif dir=='D':
            if r==numRows-1 or walls[r+1][c] == '#' or (r+2<numRows and (r+1,c) in self.blocks_source and (walls[r+2][c] == '#' or (r+2,c) in self.blocks_source)):
                return False
        elif dir=='R':
            if c==numCols-1 or walls[r][c+1] == '#' or (c+2<numCols and (r,c+1) in self.blocks_source and (walls[r][c+2] == '#' or (r,c+2) in self.blocks_source)):
                return False
        elif dir=='L':
            if c==0 or walls[r][c-1] == '#' or (c-2>=0 and (r,c-1) in self.blocks_source and (walls[r][c-2] == '#' or (r,c-2) in self.blocks_source)):
                return False
        return True

    def faux2(self, dir):
        r, c = self.player_pos
        if dir=='U':
            i = self.faux3(self.blocks_source, (r-1,c)) 
            if i!=-1:
                self.blocks_source[i] = (r-2,c)
            r -= 1 
        elif dir=='D':
            i = self.faux3(self.blocks_source, (r+1,c))
            if i!=-1:
                self.blocks_source[i] = (r+2,c)
            r += 1 
        elif dir=='R':
            i = self.faux3(self.blocks_source, (r, c+1))
            if i!=-1:
                self.blocks_source[i] = (r, c+2)
            c += 1 
        elif dir=='L':
            i = self.faux3(self.blocks_source, (r, c-1))
            if i!=-1:
                self.blocks_source[i] = (r, c-2)
            c -= 1 
        self.player_pos = (r,c)
    
    def main(self):
        m = self.margin
        
        self.easycanvas_configure(title = 'Sokoban Solution Viewer',
                                  background = 'white',
                                  size = (self.max_col*self.cell_size+m*2, self.max_row*self.cell_size+m*2), 
                                  coordinates = (-m, (self.max_row)*self.cell_size+m, (self.max_col)*self.cell_size+m, -m))
        self.draw_state(True)
        
        tt=self.create_text(self.max_col*self.cell_size/2.0,self.cell_size/2.0,"Press any key to step the solution  ",12,color="white"   ) 
        error = False
        for nd, d in enumerate(self.decisions):
            if not self.faux1(d):
                self.create_text(self.max_col*self.cell_size/2.0,self.cell_size/2.0,"Direction at pos. {0} ('{1}') not valid".format(nd, d),12,color='red')
                error = True
                break
            self.faux2(d)
            self.draw_state()
            self.update() 
            self.readkey(True)
            
        self.erase(tt)
        
        self.create_text(self.max_col*self.cell_size/2.0,(self.max_row-0.5)*self.cell_size,"Press 'Escape' key to exit",14, color='red') 
        while self.readkey(True)!='Escape':
            pass
   
def use_and_exit():
    print("Use:\n   python3 ./soboban_viewer.py <level.txt> <solution_string>")
    sys.exit() 
       
if __name__ == '__main__':
    import sys
    if len(sys.argv)!=3:
        use_and_exit()
    level_file = sys.argv[1]
    solution = sys.argv[2]

    if len(set.union(set(list(solution)), set(list('UDLR'))))>4:
        print("Invalid char on solution:", solution)
        use_and_exit()
        
    sv = SokobanViewer(level_file, solution)
    sv.run()