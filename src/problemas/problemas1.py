'''
Created on 8/10/2014

@author: al259379
'''

def squares(lista): 
    for elem in lista:
        yield elem*elem

def first(iterable, n):
    c = 0
    for elem in iterable:
        if c==n:
            break
        yield elem
        c = c + 1 

def filtra(iterable, f: "Funcion Condicional"):
    for elem in iterable:
        if f(elem):
            yield elem

def esPar(n):
    return n%2 == 0

#---------------------------------------------------------------
    
y = squares([1,3,5,7,9])

for elem in y:
    print(elem)

#---------------------------------------------------------------
 
x = squares([100,1000,10000])
a = next(x, None)
b = next(x, None)
c = next(x, None)

print(a)
print(b)
print(c)

#---------------------------------------------------------------

print(list(first(range(54,585), 6)))

#---------------------------------------------------------------

r = filtra(range(50, 200), lambda n: n<70)
print(list(r))

#---------------------------------------------------------------

s = filtra(range(10, 20),  esPar )
print(list(s))
