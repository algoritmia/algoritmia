'''
Created on 8/10/2014

@author: Jorge
'''
from algoritmia.datastructures.mergefindsets import MergeFindSet
from algoritmia.datastructures.digraphs import UndirectedGraph
from random import shuffle
from labyrinthviewer import LabyrinthViewer

filas = 20
columnas = 20

v = list((i,j) for i in range(filas) for j in range(columnas))

w = list()
for i in range(filas):
    for j in range(columnas):
        if i < filas-1:
            w.append(((i, j), (i+1, j)))
        if j < columnas-1:
            w.append(((i, j), (i, j+1)))
shuffle(w)

e = list()
m = MergeFindSet()

for elem in v:
    m.add(elem)
    
for u, v in w:
    if m.find(v) != m.find(u):
        m.merge(u, v)
        e.append((u,v))

laberinto = UndirectedGraph(E=e)
lv = LabyrinthViewer(laberinto, cell_size=30, margin=10)
lv.run()