'''
Created on 15/10/2014

@author: al259313
'''
from algoritmia.datastructures.digraphs import UndirectedGraph
from algoritmia.datastructures.mergefindsets import MergeFindSet
from algoritmia.datastructures.queues import Fifo
from labyrinthviewer import LabyrinthViewer
from random import shuffle

def recorredor_vertices_anchura(grafo, v_inicial, rows, cols):
    vertices = []
    distancias=[rows][cols]
    queue = Fifo()
    seen = set()
    queue.push((v_inicial,0))
    distancias[v_inicial[0]][v_inicial[1]]=0
    seen.add(v_inicial)
    while len(queue)>0:
        v, dist = queue.pop()
        distancias[v_inicial[0]][v_inicial[1]]=dist+1
        vertices.append(v)
        for suc in grafo.succs(v):
            if suc not in seen:
                seen.add(suc)
                queue.push((suc, dist+1))
    return distancias
     

def horse_graph(rows,cols):
    
    v=list((i,j) for i in range(rows) for j in range(cols))
    w=list()
    
    for i in range(rows):
        for j in range(cols):           
                       
            if i<rows-1 and j<cols-2:
                w.append(((i, j), (i+1, j+2)))
            if i>0 and j<cols-2:
                w.append(((i, j), (i-1, j+2)))
            if i<rows-2 and j<cols-1:
                w.append(((i, j), (i+2, j+1)))
            if i<rows-2 and j>0:
                w.append(((i, j), (i+2, j-1)))
    return w
        
w=horse_graph(8,8)
ajedrez = UndirectedGraph(E=w)
# for a in ajedrez.succs((1,7)):
#      print(a)
recorrido, dist=recorredor_vertices_anchura(ajedrez, (0,3),8,8)
print("Podemos acceder a las casillas: ")
print(recorrido)
print("Puede alcanzar cualquier casilla? ")
print(len(recorrido)==8*8)
print(dist)

w=horse_graph(2,100)
ajedrez = UndirectedGraph(E=w)
recorrido, dist=recorredor_vertices_anchura(ajedrez, (0,0),2,100)
print("Puede alcanzar cualquier casilla? ")
print(len(recorrido)==200)
print(dist)

w=horse_graph(3,100)
ajedrez = UndirectedGraph(E=w)
recorrido, dist=recorredor_vertices_anchura(ajedrez, (0,0),3,100)
print("Puede alcanzar cualquier casilla? ")
print(len(recorrido)==300)
print(dist)


