'''
Created on Nov 05, 2014

@author: Aitor Algorta
'''
from partialsolution import PartialSolution 

def primera_libre(mat): 
    for fila in range(9):
        for col in range(9):
            if mat[fila][col] == 0:
                return (fila,col)
    return None
            
def posibles_en(mat, fila, col): 
    used = set(mat[fila][c] for c in range(9))
    used = used.union(set(mat[f][col] for f in range(9)))
    fc, cc = fila//3, col//3
    used = used.union(set(mat[fc*3+f][cc*3+c] for f in range(3) for c in range(3)))
    return [num for num in range(1,10) if num not in used]


def sudoku_solver(matriz_sudoku):
    class SudokuPS(PartialSolution):
        
        def __init__(self, matriz_sudoku):
            self.matriz = matriz_sudoku
            
        def is_solution(self)-> "bool":
            if primera_libre(self.matriz) == None:
                return True
            return False
    
        def get_solution(self)  -> "solution":
            return self.matriz
    
        def successors(self) -> "IEnumerable<PartialSolution>":
                row, col = primera_libre(self.matriz)
                lista_posibles = posibles_en(self.matriz, row, col)
                for i in range(len(lista_posibles)):
                    m2 = []
                    for elem in self.matriz:
                        m2.append(elem[:])
                    m2[row][col] = lista_posibles[i]
                    yield SudokuPS(m2)
        
    def bt(ps):
        if ps.is_solution():  
            yield  ps.get_solution()  
        else:  
            for new_ps in ps.successors():  
                yield  from  bt(new_ps)
    
    initial_ps = SudokuPS(matriz_sudoku)
    yield from bt(initial_ps)
    
matriz_sudoku=[[0,0,0,3,1,6,0,5,9],[0,0,6,0,0,0,8,0,7],[0,0,0,0,0,0,2,0,0],
               [0,5,0,0,3,0,0,9,0],[7,9,0,6,0,2,0,1,8],[0,1,0,0,8,0,0,4,0],
               [0,0,8,0,0,0,0,0,0],[3,0,9,0,0,0,6,0,0],[5,6,0,8,4,7,0,0,0]]

solutions = list(sudoku_solver(matriz_sudoku))
if len(solutions)>0:
    print ("Solutions found:", len(solutions))
    for solution in solutions:
        for fila in solution: 
            print(fila)
        print('-'*20)
else:
    print("No solution found")
