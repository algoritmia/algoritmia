'''
Created on 15/10/2014

@author: al259321
'''
from algoritmia.datastructures.digraphs import UndirectedGraph
from Problemas1.labyrinthviewer import LabyrinthViewer
from algoritmia.datastructures.queues import Fifo


def horse_graph(rows, cols):
    
    w = list()
    for i in range(rows):
        for j in range(cols):
            if i == 0 and j < cols-2:
                w.append(((i, j), (i+1, j+2)))
            if i == rows - 1 and j < cols-2:
                w.append(((i, j), (i-1, j+2)))
            if i > 0 and i < rows-1 and j < cols-2:
                w.append(((i, j), (i+1, j+2)))
                w.append(((i, j), (i-1, j+2)))
            
            if j == 0 and i < rows-2:
                w.append(((i, j), (i+2, j+1))) 
            if j == cols -1 and i < rows-2:
                w.append(((i, j), (i+2, j-1)))
            if j > 0 and j < cols-1 and i < rows-2:
                w.append(((i, j), (i+2, j+1)))
                w.append(((i, j), (i+2, j-1)))
                
    return w  


    
def recorredor_vertices_anchura(grafo, v_inicial):
    vertices = []
    queue = Fifo()
    seen = set()
    queue.push(v_inicial)

    seen.add(v_inicial)
    while len(queue)>0:
        v = queue.pop()
        vertices.append(v)
        for suc in grafo.succs(v):
            if suc not in seen:
                seen.add(suc)
                queue.push(suc)   
    return vertices

def matriz_distancias(grafo, v_inicial, rows, cols):
    distancias = []
    for i in range(0, rows):
        distancias.append([0]*cols)
    queue = Fifo()
    seen = set()
    queue.push((v_inicial, 0))
    fila = v_inicial[0]
    col = v_inicial[1]
    distancias[fila][col] = 0
    seen.add(v_inicial)
    while len(queue)>0:     
        v, dist= queue.pop()
        fila = v[0]
        col = v[1]
        distancias[fila][col] = dist
        for suc in grafo.succs(v):
            if suc not in seen:
                seen.add(suc)
                queue.push((suc, dist+1))   
    return distancias
ajedrez = UndirectedGraph(E=horse_graph(8, 8))
recorrido = recorredor_vertices_anchura(ajedrez, (0, 3))
print('Podemos acceder a las casillas')
print(recorrido)
dist = matriz_distancias(ajedrez, (0,0), 8, 8)
for fila in dist:
    print(fila)

print('Puede alcanzar cualquier casilla?')
print(len(recorrido) == 8*8)
 
ajedrez = UndirectedGraph(E=horse_graph(2, 100))
recorrido = recorredor_vertices_anchura(ajedrez, (0, 0))
print('Puede alcanzar cualquier casilla?')
print(len(recorrido) == 200)
 
ajedrez = UndirectedGraph(E=horse_graph(3, 100))
recorrido = recorredor_vertices_anchura(ajedrez, (0, 0))
print('Puede alcanzar cualquier casilla?')
print(len(recorrido) == 300)