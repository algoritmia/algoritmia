'''
Created on 29/10/2014

@author: al259321
'''
def calculaCobertura(s):
    sorted(s)
    intervalos = []
    inicio = s[0]
    fin = inicio+1
    intervalos.append((inicio, fin))
    for i in range(len(s)):
        valor = s[i]
        if valor > fin:
            inicio = valor
            fin = valor +1
            intervalos.append((inicio, fin))
    return intervalos


print(calculaCobertura([1, 2, 2.2, 3.7, 4.5, 6, 6.9]))