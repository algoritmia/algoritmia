'''
Created on Oct 24, 2014

@author: David Llorens (dllorens@uji.es)
'''
from abc import ABCMeta, abstractmethod

class PartialSolution(metaclass=ABCMeta):
    @abstractmethod
    def is_solution(self)-> "bool":
        pass
    
    @abstractmethod
    def get_solution(self)  -> "solution":
        pass
    
    @abstractmethod
    def successors(self) -> "IEnumerable<PartialSolution>":
        pass
    
class PartialSolutionWithVisitedControl(PartialSolution):
    @abstractmethod
    def state(self)-> "state": 
        # the returned object must be of an inmutable type  
        pass
    
class PartialSolutionWithOptimization(PartialSolutionWithVisitedControl):
    @abstractmethod
    def f(self)-> "int or double":   
        # result of applying the objective function to the partial solution
        pass