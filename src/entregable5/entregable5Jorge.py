'''
Created on 11/12/2014

@author: Jorge
'''
import sys
 
def loadfile(filename):
    try:    
        file = open(filename, "r").read()
    except:
        print("Problem reading file level:", filename)
        sys.exit()
    alturas, puntos = list(), list()
    for line in file.split('\n'):
        if(line != ''):
            datos = line.split(' ')
            alturas.append(int(datos[0]))
            puntos.append(int(datos[1]))
    return (alturas, puntos)

def puntuacion_maxima(alturas, puntos):
#     He fet una funció que fa lo mateix que laltra pero en n-1, que senten molt millor
#     Per a que funcione sha de canviar el max per un min a lhora de cridarla
#     Crec que es una millor implementacio. Mireula

#     def F(n, altura):
#         if  n == 0: return 0
#         if (n, altura) not in mem:
#             if alturas[globos-n] > altura: mem[n, altura] = (F(n - 1, altura), altura, 0)
#             else: mem[n, altura] = max((F(n-1, altura), altura,  0), (F(n-1, alturas[globos-n]) + puntos[globos-n], alturas[globos-n], 1))
#         return mem[n, altura][0]
    
    def F(n, altura):
        if  n == 0: return 0
        if (n, altura) not in mem:
            if alturas[n-1] < altura: mem[n, altura] = (F(n - 1, altura), altura, 0)
            else: mem[n, altura] = max((F(n-1, altura), altura,  0), (F(n-1, alturas[n-1]) + puntos[n-1], alturas[n-1], 1))
        return mem[n, altura][0]
    
    mem = {}
    globos = len(alturas)
    puntuacion = (F(globos, min(alturas)))
    sol = []
    n, altura = globos, min(alturas)
    while n != 0:
        (puntos, altura_previa, usado) = mem[n, altura]
        sol.append(usado)
        n, altura = n-1, altura_previa
    sol.reverse() 
    
    solucion_final = list()
    for i in range(len(sol)):
        if sol[i] == 1: solucion_final.append(i+1)

    return puntuacion, solucion_final
    

alturas, puntos = loadfile(sys.argv[1])

puntos, recorrido = puntuacion_maxima(alturas, puntos)
print(puntos)
for elem in recorrido:
    print(elem, end=" ")


