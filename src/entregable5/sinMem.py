'''
Created on 22/12/2014

@author: aalgorta
'''

import sys

def loadfile(filename):
    try:    
        file = open(filename, "r").read()
    except:
        print("Problem reading file level:", filename)
        sys.exit()
    alturas, puntos = list(), list()
    for line in file.split('\n'):
        if(line != ''):
            datos = line.split(' ')
            alturas.append(int(datos[0]))
            puntos.append(int(datos[1]))
    return (alturas, puntos)

def puntuacion_maxima(alturas, puntos):
    def F(n, altura):
            if  n == 0: return 0
            if alturas[n-1] < altura: return F(n - 1, altura)
            return max(F(n-1, altura), F(n-1, alturas[n-1]) + puntos[n-1])
    globos = len(alturas)
    puntuacion = (F(globos, min(alturas)))
    return puntuacion
            

alturas, puntos = loadfile(sys.argv[1])

puntos, recorrido = puntuacion_maxima(alturas, puntos)
print(puntos)
for elem in recorrido:
    print(elem, end=" ")
